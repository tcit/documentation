# Manage actors
## List all available commands

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl actors
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl actors
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.actors
    ```

## Show an actors details

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl actors.show <actor_username>
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl actors.show <actor_username>
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.actors.show <actor_username>
    ```

Where `<actor_username>` is a local or federated, profile or group, such as `myprofile`, `mygroup`, `remote_profile@remote_instance.com` or `mygroup@my_remote_instance.com`.

In addition to basic informations, it also tells which user the profile belongs to, if local.

## Refresh a remote actor

Can be helpful in case of federation issues.

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl actors.refresh <federated_group_name>
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl actors.refresh <federated_group_name>
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.actors.refresh <federated_group_name>
    ```

Where `<federated_group_name>` is a federated group name like `mygroup@my_remote_instance.com`.