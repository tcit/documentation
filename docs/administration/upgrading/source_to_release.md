# Migrate from Source install to Release install

The 1.1 version of Mobilizon brings Elixir releases support. An Elixir release is a self-contained directory that contains all of Mobilizon's code (front-end and backend), it's dependencies, as well as the Erlang Virtual Machine and runtime (only the parts you need). As long as the release has been assembled on the same OS and architecture, it can be deploy and run straight away. [Read more about releases](https://elixir-lang.org/getting-started/mix-otp/config-and-releases.html#releases).

We provide a small script to handle things like open the interactive shell, restart the release or execute mix tasks.

With releases, we adopt a more traditional way of managing configuration and media files. 

* **Configuration is now assumed to be in `/etc/mobilizon/config.exs` by default**.  
  You can change the path with the `MOBILIZON_CONFIG_PATH` environment variable. As this file holds Mobilizon only read-access to this file.

* **Uploads are now assumed to be in `/var/lib/mobilizon/uploads` by default**.
  You can change the path in your config file, with the following config:
  ```elixir
  config :mobilizon, Mobilizon.Web.Upload.Uploader.Local, uploads: "/var/lib/mobilizon/uploads`
  ```

* **The release files will be assumed to located in `/opt/mobilizon` by default**.  
  Feel free to put them elsewhere and adapt the following instructions, like `/usr/local/lib/mobilizon`.

These steps are to be executed as root or with `sudo`, in your mobilizon current folder:

Start by stopping the Mobilizon process:
```sh
systemctl stop mobilizon
```

## Download and install

!!! info
    To get the latest package URL, you should go to [our release page](https://framagit.org/framasoft/mobilizon/-/releases) and copy the URL from the latest `tar.gz` package.
    
    The https://joinmobilizon.org/latest-package URL redirects to [the latest <ins>stable</ins> release package available](https://joinmobilizon.org/latest-release) ([Gitlab doesn't allow to do this directly currently](https://gitlab.com/gitlab-org/gitlab/-/issues/16821)), but you shouldn't use it it you want to get a beta version or a different version.

* Download the latest release build and unzip it:  
  ```sh
  curl -L 'https://joinmobilizon.org/latest-package' -o /tmp/mobilizon.tar.gz
  tar xzf /tmp/mobilizon.tar.gz -C /tmp/
  ```
* Move the release to the install folder:  
  ```sh
  mv /tmp/mobilizon /opt/
  rm /tmp/mobilizon.tar.gz
  ```

* Set the proper owner to `/opt/mobilizon`
  ```sh
  chown -R mobilizon:mobilizon /opt/mobilizon
  ```
## Move the configuration

* Create the config directory
  ```sh
  mkdir -p /etc/mobilizon
  ```

* Copy your current configuration to the new location:
!!! important "Configuration file"
    Your configuration file might be either in `config/prod.secret.exs` or `config/runtime.exs`. Make sure to copy the correct file.

  ```bash
  cp config/prod.secret.exs /etc/mobilizon/config.exs
  ```

## Adapt the configuration

!!! warning "Configuration header"
    Make sure the top of the config file starts with `import Config` and not `use Mix.Config`. If not, replace it.

If your config file doesn't includes `server: true` under `Mobilizon.Web.Endpoint`, add it.
    ```diff
    config :mobilizon, Mobilizon.Web.Endpoint,
    + server: true,
    ```

## Move the media uploads
* Create the media directory
  ```sh
  mkdir -p /var/lib/mobilizon/uploads
  ```

* Move all uploads to the new location:  
  ```bash
  mv uploads/* /var/lib/mobilizon/uploads
  ```
* Make sure the uploaded files have the proper owner:  
  ```bash
  chown -R mobilizon:mobilizon /var/lib/mobilizon/uploads
  ```

## Execute pending database migrations
You may have updated to a newer version, so let's run database migrations just to be sure:
```bash
sudo -u mobilizon /opt/mobilizon/bin/mobilizon_ctl migrate
```

## Test that the instance starts properly

Run Mobilizon as a daemon to test the startup
```bash
/opt/mobilizon/bin/mobilizon daemon && tail tmp/log/erlang.log.1 -f
```
If the output contains the following `Access Mobilizon.Web.Endpoint at https://yourinstance.com` **without any error messages**, it should be fine. Otherwise your configuration is probably incorrect and you need to check it.

Either way, exit the `tail -f` with ++ctrl+c++ then stop the daemon with:
```
/opt/mobilizon/bin/mobilizon stop
```

## Edit the nginx configuration

* Edit the nginx virtual host file to change paths:
  ```
  $EDITOR /etc/nginx/sites-enabled/mobilizon.conf
  ```
  ```diff
  - /home/mobilizon/live/priv/static
  + /opt/mobilizon/priv/static

  - /home/mobilizon/live/priv/errors
  + /opt/mobilizon/priv/errors
  ```

* Test that configuration is correct and reload nginx
  ```sh
  nginx -t && systemctl reload nginx
  ```

## Edit the systemd configuration

* Edit the systemd unit file to change the path to mobilizon:
  ```bash
  $EDITOR /etc/systemd/system/mobilizon.service
  ```
  And change the following lines:
  ```diff
  - WorkingDirectory=/home/mobilizon/live
  - ExecStart=/usr/bin/env mix phx.server
  - ExecReload=/bin/kill $MAINPID
  + WorkingDirectory=/opt/mobilizon
  + ExecStart=/opt/mobilizon/bin/mobilizon start
  + ExecStop=/opt/mobilizon/bin/mobilizon stop
  ```
* Reload systemd
  ```bash
  systemctl daemon-reload
  ```

* Finally, restart the Mobilizon service
  ```bash
  systemctl restart mobilizon
  ```

## Remove the source install

Once we're confident everything works properly, we can get rid of the source install.

* Go to the `mobilizon`'s user `$HOME` directory and remove the `live` directory and dependencies cache.
  ```bash
  cd /home/mobilizon
  rm -r live/ .cache/ .hex/ .mix/ .yarn/
  ```

* Move `mobilizon`'s `$HOME` directory to the new mobilizon root folder.
  ```sh
  usermod -m -d /opt/mobilizon mobilizon
  ```
