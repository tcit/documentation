# Page d'un groupe

## Rejoindre un groupe

Si le groupe est [configuré pour permettre à n'importe qui de rejoindre le groupe sans invitation](../../groupes/creation-groupe/#parametres), vous devez, sur la page du groupe, cliquer sur le bouton **Rejoindre le groupe**. Si vous n'êtes pas connecté⋅e à votre compte, vous serez invité⋅e à le faire.

## Signaler un groupe

Pour signaler un groupe, vous devez&nbsp;:

  1. cliquer sur le bouton **⋅⋅⋅**
  * cliquer sur le bouton **Signalement**

    ![bouton de signalement](../../images/report-group-FR.png)

  * [Optionnel mais **recommandé**] renseigner la raison du signalement&nbsp;:
    ![modale de signalement](../../images/report-group-modal-FR.png)
