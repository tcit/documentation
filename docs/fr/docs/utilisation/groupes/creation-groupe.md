# Comment créer et paramétrer un groupe

Les groupes sont des espaces de coordination et de préparation. Ils peuvent être utilisés pour améliorer l'organisation d'événements et pour gérer votre communauté.

## Création

=== "Comment faire"
    Pour créer un groupe, vous devez&nbsp;:

    1. cliquer sur **Mes groupes** dans le menu supérieur
    * cliquer sur le bouton **Créer un groupe**
    * (requis) indiquer un **nom d'affichage de groupe**
    * (requis) indiquer un **nom fédéré de groupe**&nbsp;: c'est comme votre addresse fédérée (`vous@instance-mobilizon.tld`) pour les groupes. Cela vous permettra d'être trouvable sur la fédération, et est garanti d'être unique.
    * entrer une description de votre groupe
    * ajouter un avatar pour votre groupe en sélectionnant une image sur votre appareil
    * ajouter une image d’illustration n sélectionnant une image sur votre appareil
    * cliquer sur le bouton **Créer mon groupe**

=== "Capture d'écran"
    ![creation group img](../../images/group-creation-FR.png)

## Paramètres

=== "Comment faire"
    Vous pouvez accéder aux paramètres de votre compte en&nbsp;:

    1. cliquant sur **Mes groupes** dans le menu supérieur
    * cliquant sur le groupe souhaité
    * cliquant sur le bouton **Paramètres du groupe** dans l'image d'illustration

    Dans cette section vous pouvez modifier les informations ajoutées à la création du groupe (voir ci-dessus) mais vous pouvez aussi&nbsp;:

    * changer la visibilité du groupe&nbsp;:
        * **Visible partout sur le web**&nbsp;: le groupe sera listé publiquement dans les résultats de recherche et pourra être suggéré sur la page « Explorer ». Seules les informations publiques seront affichées sur sa page.
        * **Accessible uniquement par le lien**&nbsp;: Vous aurez besoin de transmettre l'URL du groupe pour que d'autres personnes accèdent au profil du groupe. Le groupe ne sera pas trouvable dans la recherche de Mobilizon ni dans les moteurs de recherche habituels.
    * Permettre de rejoindre ou non le groupe&nbsp;:
        * **N'importe qui peut rejoindre**&nbsp;: n'importe qui voulant devenir membre pourra le faire depuis votre page de groupe.
        * **Inviter des nouveaux·elles membres manuellement**&nbsp;: la seule manière pour votre groupe d'obtenir de nouveaux·elles membres sera si un·e administrateur·ice les invite.
    * ajouter l'**adresse du groupe**

=== "Capture d'écran"
    ![modification des paramètres du groupe](../../images/group-creation-settings-FR.png)
